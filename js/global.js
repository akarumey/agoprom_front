$(document).ready(function() {
    var prevarrow = '<div class="slick-prev-arrow"> <i class="fas fa-chevron-left"></i> </div>';
    var nextArrow = '<div class="slick-next-arrow"> <i class="fas fa-chevron-right"></i> </div>';

    $(".product-filter-item.closed").find(".filter-item-checkbox").slideUp(600);

    //Header MENU

    $(".header-mobile-search").on('click', function () {
        var menu = $(this).parents(".row").find(".header-main-item.search");

        if($(this).hasClass('opened')){
            $(menu).slideUp(300);
            $(this).removeClass('opened');
        } else if(!$(this).hasClass('opened')){
            $(this).addClass('opened');
            $(menu).slideDown(300);
        }
    });

    $(".header-mobile-nav").on('click', function () {
        var menu = $(this).parent().find(".header-nav-menu");

        if($(this).hasClass('opened')){
           $(menu).slideUp(300);
           $(this).removeClass('opened');
        } else if(!$(this).hasClass('opened')){
            $(this).addClass('opened');
            $(menu).slideDown(300);
        }
    });

    //Footer MENU

    $(".footer-content.toggle").on('click', function () {
        var menu = $(this).find(".toggle-item");
        var plus = $(this).find('fa-plus');
        var minus = $(this).find('fa-minus');

        if ($(window).width() <= 1200){
            if($(this).hasClass('opened')){
                $(menu).slideUp(300);
                $(this).removeClass('opened');
            } else if(!$(this).hasClass('opened')){
                $(".toggle-item").slideUp(300);
                $(".footer-content.toggle").removeClass('opened');
                $(this).addClass('opened');
                $(menu).slideDown(300);
            }
        } else {
            return 0;
        }
    });

    //Go TOP
    $(window).scroll(function () {
        if ($(this).scrollTop() > 400) {
            $('.go_top_block').fadeIn();
        } else {
            $('.go_top_block').fadeOut();
        }
    });

    $('.go_top').on('click', function(){
        $('html, body').animate({ scrollTop:0 }, 1500);
    });

    //Filter

    $(".product-mobile-filter").on('click', function () {
        var menu = $(this).parent().find(".product-filter-container");

        if($(this).hasClass('opened')){
            $(menu).slideUp(300);
            $(this).removeClass('opened');
        } else if(!$(this).hasClass('opened')){
            $(this).addClass('opened');
            $(menu).slideDown(300);
        }
    });

    $(".filter-item-title").on('click', function () {
        var parent = $(this).parent();
        var content = $(parent).find(".filter-item-checkbox");

        if(!$(parent).hasClass("closed")){
            $(parent).addClass("closed");
            $(content).slideUp(600);
        } else if ($(parent).hasClass("closed")){
            $(parent).removeClass("closed");
            $(content).slideDown(600);
        }
    });

    //Sliders
    if($("div").is('.main-slider')){
        $('.main-slider .slick-slider').slick({
            dots: true,
            customPaging : function(slider, i) {
                var thumb = $(slider.$slides[i]).data();
                return '<a class="slick-dots-item">'+ '0' + (i+1) +'</a>';
            },
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1
        });
        $(".main-slider .slick-dots").wrap("<div class=\"container slick-dots-container\"></div>");
    } else {}

});

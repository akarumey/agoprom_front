//UI Slider

$("#slider").slider({

    min: 0,
    max: 900000,
    values: [0,900000],
    range: true,
    stop: function(event, ui) {
        $("input#minCost").val($("#slider").slider("values",0));
        $("input#maxCost").val($("#slider").slider("values",1));
    },
    slide: function(event, ui){
        $("input#minCost").val($("#slider").slider("values",0));
        $("input#maxCost").val($("#slider").slider("values",1));
    }

});

$("input#minCost").change(function(){
    var value1=$("input#minCost").val();
    var value2=$("input#maxCost").val();

    if(parseInt(value1) > parseInt(value2)){
        value1 = value2;
        $("input#minCost").val(value1);
    }

    $("#slider").slider("values",0,value1);
});


$("input#maxCost").change(function(){
    var value1=$("input#minCost").val();
    var value2=$("input#maxCost").val();

    if (value2 > 900000) { value2 = 900000;
        $("input#maxCost").val(900000)}

    if(parseInt(value1) > parseInt(value2)){
        value2 = value1;
        $("input#maxCost").val(value2);
    }

    $("#slider").slider("values",1,value2);
});